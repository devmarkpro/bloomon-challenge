# Bloomon Challenge

[![pipeline status](https://gitlab.com/devmark88/bloomon-challenge/badges/master/pipeline.svg)](https://gitlab.com/devmark88/bloomon-challenge/commits/master)

My way to solve `Bloomon` code challenge

## Build and Run

This project is based on *Typescript* so you can run the project after build it or run directory from typescript by `ts-node`

- `yarn` or `npm install` to install all dependencies
- run `yarn start` to build and start the project
- Alternatively you can start project via `yarn dev` to run project directly in *Typescript*
- pass `--input` param to send the input file to the application eg: `yarn start --input ../input.txt`

## Tests

For run the test simply run `yarn test` command. you can see the html coverage in the folder called `coverage` in the root of the project.

## Docker

The application is dockerize! so you can simply build and run the docker image.

### Build image by yourself

- `yarn install && yarn build` to install dependencies and build project
- `docker build -t bloomon-mark-challenge .` to create docker image from project
- `docker run -v ~/Desktop/inputs:/data bloomon-mark-challenge` to run and send your input file

### Get from repository

This project has `CI` process. Each `push` in master branch run pipeline and through the pipeline `gitlab` build and push the image in `gitlab registry`. So you can simply pull the docker image from that registry.

- run `docker login registry.gitlab.com` with an account that has access to this repository.
- `docker run -v ~/Desktop/inputs:/data bloomon-mark-challenge` to run and send your input file

This repository is mirrored to github [private repository](https://github.com/devmark88/blomoon-challenge)

PS: Why I didn't install dependencies and build the project in Dockerfile?
I use `gitlab ci` for `CI` and through the steps of it, I run `yarn install` and `yarn build` to install dependencies and build the project. In this way I can put artifacts in the cache to repeat the process fastly.

PS: Unfortunately back to special situation that I had, I couldn't find a chance to work on the project 4 hours in row. but all the time that I spent in this project is less than 4 hours. you can track the time and development process from my commits if you want. 

### TODO

The list of works that I can do if I had more time.

- [ ] Add more test.
- [ ] Better commenting.
- [ ] Add log verbosity as input param.
- [ ] Add more log based on verbosity.

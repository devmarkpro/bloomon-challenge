FROM node:10.15.0-alpine

ARG BUILD_DATE
ARG COMMIT_REF_SLUG
ARG COMMIT_SHORT_SHA

LABEL maintainer="mark.karamyar@gmail.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="Bloomon Challenge" \
    org.label-schema.description="Bloomon Challenge" \
    org.label-schema.url="https://gitlab.com/devmark88/bloomon-challenge" \
    org.label-schema.vcs-url="https://gitlab.com/devmark88/bloomon-challenge" \
    org.label-schema.vcs-ref=$COMMIT_SHORT_SHA \
    org.label-schema.vendor="Bloomon" \
    org.label-schema.version=$COMMIT_REF_SLUG

WORKDIR /app

COPY ./dist ./dist
COPY ./node_modules ./node_modules/
COPY ./package.json ./package.json
RUN ls /app/dist
CMD ["sh", "-c", "node /app/dist/bundle.js --input /data/input.txt"]

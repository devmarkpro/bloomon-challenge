import { Bouquet } from '../Models/Bouquet';

let bouquets: Bouquet[] = [];

/**
 * Bouquet storage. Keep bouquets input stream to the temp storage
 */
export class BouquetStorage {
  /**
   * Adds bouquet storage
   * @param {Bouquet} bouquet
   * @returns {BouquetStorage} add. Add an item to the storage
   */
  public add(bouquet: Bouquet): BouquetStorage {
    if (!bouquet) {
      return this;
    }
    if (!bouquets.find((b: Bouquet) => b.id === bouquet.id)) {
      bouquets.push(bouquet);
    }
    return this;
  }

  /**
   * Get first of bouquet storage
   * @returns {Bouquet} getFirst.
   */
  public getFirst = (): Bouquet => {
    return bouquets[0];
  };

  /**
   * Get  of bouquet storage
   * @returns {Array<Bouquet>}
   */
  public get = (): Bouquet[] => {
    return bouquets;
  };

  /**
   * Get count of bouquet storage
   * @returns {number}
   */
  public getCount = (): number => {
    return bouquets.length;
  };

  /**
   * Clear  of bouquet storage
   */
  public clear = () => {
    bouquets = [];
  };

  /**
   * Remove an item of bouquet storage
   * @param {Bouquet} bouquet. The item that will remove from storage
   * @returns {BouquetStorage}
   */
  public remove = (bouquet: Bouquet): BouquetStorage => {
    const idx = bouquets.findIndex(a => a.id === bouquet.id);
    if (idx >= 0) {
      bouquets.splice(idx, 1);
    }
    return this;
  };
}

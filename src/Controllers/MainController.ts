import { Bouquet } from '../Models/Bouquet';
import { Flower } from '../Models/Flower';
import { FlowerStorage } from '../Storage/FlowerStorage';
import { BouquetStorage } from '../Storage/BouquetStorage';
import OutputService from '../Services/BouquetOutputService';
let seeNewLine = false;
const flowerStorage = new FlowerStorage();
const bouquetStorage = new BouquetStorage();

/**
 * The main processor of the application
 * @returns {Function} process. is the process function
 */
export default () => {
  return {
    /**
     * Process input by line, add flowers and bouquets to the storage and make decision about the output
     * @param {string} line the input from the stream
     */
    process: (line: string) => {
      try {
        if (!seeNewLine) {
          const bouquet = Bouquet.Parse(line);
          if (!bouquet) {
            seeNewLine = true;
          }
          bouquetStorage.add(bouquet!);
        } else {
          flowerStorage.add(Flower.Parse(line)!);
        }
        OutputService.process({
          FlowerStorage: flowerStorage,
          BouquetStorage: bouquetStorage,
        });
      } catch (error) {
        process.stderr.write(error + '\n');
        process.exit(1);
      }
    },
  };
};

import Dictionary from 'Dictionary';
import { IllegalArgumentException } from '../Errors';

/**
 * Gets commands. Create CommandArg model
 * @param {Array<string>} args input arguments from process
 * @returns {CommandArguments} commands.
 */
export const getCommands = (args: string[]): CommandArguments => {
  const commands: Dictionary<string> = Object.create({});
  args.forEach((val: string, index: number, array: string[]) => {
    if (val === '--input') {
      commands[val.substr(2)] = array[index + 1];
    }
    if (val === '--output') {
      commands[val.substr(2)] = array[index + 1];
    }
  });
  const result: CommandArguments = {
    input: '',
  };
  if (commands.input) {
    result.input = commands.input;
  } else {
    throw new IllegalArgumentException(
      'Please provide input file with "--input" option. exp: yarn dev --input ./sample/input.txt',
      'input',
    );
  }
  if (commands.output) {
    result.output = commands.output;
  }
  return result;
};

export interface CommandArguments {
  input: string;
  output?: string;
}

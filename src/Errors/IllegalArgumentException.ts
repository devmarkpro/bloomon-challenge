export default class IllegalArgumentException extends Error {
  constructor(message: string | undefined, ...params: string[]) {
    let msg: string = message
      ? message + ' => ' + ' Missing Arguments: '
      : 'Missing Arguments: ';
    params.forEach((p: string) => (msg += p + ' -'));
    super(msg);
    Error.captureStackTrace(this);
    this.name = 'IllegalArgumentException';
    this.message = msg;
  }
}

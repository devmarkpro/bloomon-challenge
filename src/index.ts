import { createReadStream } from 'fs';
import { config as dotenv } from 'dotenv';
import { createInterface } from 'readline';
import MainController from './Controllers/MainController';
import { getCommands } from './Utils/CommandHelper';

const args = getCommands(process.argv);
dotenv();
const controller = MainController();

const lineReader = createInterface({
  input: createReadStream(args.input),
  output: process.stdout,
});
process.stdin.setEncoding('utf-8');
lineReader.on('line', controller.process);

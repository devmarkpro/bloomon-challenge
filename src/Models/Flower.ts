import { ReadLine } from 'readline';
import { InvalidLineException } from '../Errors';
import AppLogger from '../Utils/AppLogger';

/**
 * Flower
 */
export class Flower {
  /**
   * Parses flower Convert input string to flower object
   * @param {string} line the input from the stream
   * @returns {Flower|null} result is the object which export from the input
   */
  public static Parse(line: string): Flower | null {
    if (!line) {
      return null;
    }
    if (!line[0] || !line[1]) {
      throw new InvalidLineException(
        'Cannot get size or name of the Bouquet',
        line,
      );
    }
    if (line.length > 2) {
      AppLogger.warn(
        `"${line}" is not valid as flower code. The rest of characters after "${line[0] +
          line[1]}" was ignored.`,
      );
    }
    return new Flower(line[0], line[1]);
  }

  public id: string;
  public name: string;
  public size: string;
  /**
   * Creates an instance of flower.
   * @param {string} name
   * @param {string} size
   */
  constructor(name: string, size: string) {
    this.id = `${name}:${size}`;
    this.name = name;
    this.size = size;
  }
}

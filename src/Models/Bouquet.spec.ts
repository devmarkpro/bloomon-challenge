import { Bouquet } from './Bouquet';
import FlowerQuantity from 'FlowerCount';
import { InvalidLineException } from '../Errors';

describe('*** parse bouquet', () => {
  it('should parse correctly', () => {
    const sample = 'AL10a15b5c30';
    const bouquet = Bouquet.Parse(sample);

    expect(bouquet).not.toBeNull();
    if (bouquet) {
      expect(bouquet instanceof Bouquet).toBeTruthy();
      expect(bouquet.flowers).not.toBeNull();
      expect(bouquet.flowers.length).toEqual(4);

      expect(bouquet.flowers[0].count).toEqual(10);
      expect(bouquet.flowers[0].flower.name).toMatch('a');
      expect(bouquet.flowers[0].flower.size).toMatch('l');

      expect(bouquet.flowers[1].count).toEqual(15);
      expect(bouquet.flowers[1].flower.name).toMatch('b');
      expect(bouquet.flowers[1].flower.size).toMatch('l');

      expect(bouquet.flowers[2].count).toEqual(5);
      expect(bouquet.flowers[2].flower.name).toMatch('c');
      expect(bouquet.flowers[2].flower.size).toMatch('l');

      expect(bouquet.flowers[3].count).toEqual(30);
      expect(bouquet.flowers[3].flower.name).toMatch('Other');
      expect(bouquet.flowers[3].flower.size).toMatch('l');

      expect(bouquet.name).toMatch('A');
      expect(bouquet.size).toMatch('L');
    }
  });
  it('should return null for empty input', () => {
    const sample = '';
    expect(Bouquet.Parse(sample)).toBeNull();
  });
  it('should throw exception for bad input', () => {
    const sample = '&^';
    expect(() => Bouquet.Parse(sample)).toThrowError(InvalidLineException);
  });
});

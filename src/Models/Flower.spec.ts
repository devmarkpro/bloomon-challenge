import { Flower } from './Flower';
import { InvalidLineException } from '../Errors';

describe('*** parse flower', () => {
  let outputData = '';
  const storeLog = (inputs: string) => (outputData += inputs);
  it('should parse correctly', () => {
    const sample = 'as';
    const flower = Flower.Parse(sample);
    expect(flower).not.toBeNull();
    if (flower) {
      expect(flower instanceof Flower).toBeTruthy();
      expect(flower.name).toMatch('a');
      expect(flower.size).toMatch('s');
    }
  });
  it('should return null for empty input', () => {
    const sample = '';
    expect(Flower.Parse(sample)).toBeNull();
  });
  it('should throw exception for bad input', () => {
    const sample = '1';
    expect(() => Flower.Parse(sample)).toThrowError(InvalidLineException);
  });
  it('should parse flowers if line exceeded', () => {
    const sample = 'as03945';
    const flower = Flower.Parse(sample);
    expect(flower).not.toBeNull();
    if (flower) {
      expect(flower instanceof Flower).toBeTruthy();
      expect(flower.name).toMatch('a');
      expect(flower.size).toMatch('s');
    }
  });
});

import { FlowerStorage } from '../Storage/FlowerStorage';
import { BouquetStorage } from '../Storage/BouquetStorage';
import { Bouquet } from '../Models/Bouquet';
import AppLogger from '../Utils/AppLogger';

/**
 * Storage model
 */
export interface StorageModel {
  FlowerStorage: FlowerStorage;
  BouquetStorage: BouquetStorage;
}

export default {
  /**
   *
   * @param {StorageModel} storage an object which has both Flower and Bouquet storages.
   */
  process: (storage: StorageModel) => {
    // const firstRemainBouquet = storage.BouquetStorage.getFirst();
    storage.BouquetStorage.get().forEach((firstRemainBouquet: Bouquet) => {
      const flowers = storage.FlowerStorage.get();
      const result = firstRemainBouquet.flowers
        .filter(a => a.flower.name.toLowerCase() !== 'other')
        .every(a => {
          const flowerCount = flowers
            .filter(
              m =>
                m.flower.id.toLocaleLowerCase() === a.flower.id.toLowerCase() &&
                a.flower.name.toLowerCase() !== 'other',
            )
            .map(o => o.count);

          if (flowerCount.length > 0) {
            const sum = flowerCount.reduce((x, y) => x + y);

            return sum >= a.count;
          }
          return false;
        });
      if (result) {
        AppLogger.info(`${firstRemainBouquet.spec} is ready now!\n`);
        storage.BouquetStorage.remove(firstRemainBouquet);
        storage.FlowerStorage.closeBouquet(firstRemainBouquet);
      } else {
        AppLogger.debug(
          `sorry, ${
            firstRemainBouquet.id
          } is not ready yet! We're trying hard to process all flowers as fast as possible but for now you should wait a little bit\n`,
        );
      }
    });
  },
};
